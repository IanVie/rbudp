import java.awt.List;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Timer;

public class Reciever {

	private static DatagramSocket socket = null;
	private BufferedReader in = null;
	private static ObjectOutputStream os;
	private static ArrayList<Integer> rec;
	private static byte[] full; 
	protected static boolean drop;

	public static void main(String[] args) throws IOException, InterruptedException {

		socket = new DatagramSocket(8001);
		drop = true;

		ServerSocket serverSocket = new ServerSocket(8003);
		Socket tcpSocket = serverSocket.accept();
		os = new ObjectOutputStream(tcpSocket.getOutputStream());
		full = new byte[10000000];
		while (true) {
			RecDG();
			os.writeObject(rec);
		}
	}

	public static void RecDG() {
		int bytCheck = 0;
		int seqNo = 0;
		byte[] buf;
		DatagramPacket packet;
		rec = new ArrayList<Integer>();
		long startT = System.currentTimeMillis();
		long elapsed = 0;
		
		while (elapsed < 50) {

			try {
				buf = new byte[1002];

				packet = new DatagramPacket(buf, buf.length);
				socket.receive(packet);
				elapsed = System.currentTimeMillis() - startT;
				//System.out.println(elapsed);

				if (packet.getLength() == 0) {
					break;
				}

				if ((buf[0] & 0xFF) > 0) {
					seqNo = (buf[0] & 0xFF) * 256 + (buf[1] & 0xFF);
				} else {
					seqNo = (buf[1] & 0xFF);
				}

				rec.add(seqNo);
				for (int i = 0; i < 1000; i ++){
					full[seqNo*1000 + i] = packet.getData()[i];
				}
				String received = new String(packet.getData(), 0, packet.getLength());
				//System.out.println(received);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("END");
	}
}