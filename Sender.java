import java.awt.FlowLayout;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

public class Sender {

	private static FileInputStream fis = null;
	private static ObjectInputStream ois;
	private static JProgressBar pb;
	private static DatagramSocket socket;
	private static ArrayList<Integer> notRec;
	private static InetAddress address;
	private static long fSize;
	private static File file;
	private static ArrayList<Integer> sent;
	private static int packets;
	private static boolean drop;// number of dropped packets

	public static void main(String s[]) throws IOException, InterruptedException, ClassNotFoundException {

		final JFrame frame = new JFrame("File Trans");

		// creates progress bar
		pb = new JProgressBar();
		pb.setMinimum(0);
		pb.setMaximum(22);
		pb.setStringPainted(true);

		// add progress bar
		frame.setLayout(new FlowLayout());
		frame.getContentPane().add(pb);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 200);
		frame.setVisible(true);

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("choosertitle");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);

		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			
			file = new File(chooser.getSelectedFile().toString());
			fis = new FileInputStream(file);
			socket = new DatagramSocket();
			address = InetAddress.getByName("localhost");
			fSize = fis.getChannel().size();
			notRec = new ArrayList<Integer>();
			sent = new ArrayList<Integer>();
			packets = (int) Math.ceil(fSize / 1000) + 1;

			try {
				Socket tcpSocket = new Socket("localhost", 8003);
				ois = new ObjectInputStream(tcpSocket.getInputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < packets + 1; i++) {
				notRec.add(i);
			}

			drop = true;
			while (drop) {
				SendDG();
				RecTCP();
			}
			System.out.println("------------------- SEND SUCCESSFUL --------------------");
		}
	}

	public static void SendDG() throws IOException, InterruptedException {
		
		sent.clear();
		if (notRec.size() == 0) {
			drop = false;
		} else {			
			byte[] buf = new byte[1002];
			for (int i : notRec) {
				final int currentValue = i;
				buf[0] = (byte)((int)(i/256));
				byte b = (byte) i;
				buf[1] = b;
				fis.skip(i*1000);
				fis.read(buf, 2, 1000); // read from 1 offset
				sent.add(i); //keep track of sent packets
				DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 8001);
				socket.send(packet);

				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						pb.setValue(currentValue);
					}
				});
			}

			Thread.sleep(100);
			byte[] finalbuf = new byte[0];
			DatagramPacket lastpacket = new DatagramPacket(finalbuf, 0, address, 8001);
			socket.send(lastpacket);
			notRec.clear();
			System.out.println("Packet Sent \n");
			fis = new FileInputStream(file);

		}

	}

	public static void RecTCP() throws ClassNotFoundException, IOException {

		Object object = ois.readObject();
		ArrayList<Integer> rec = (ArrayList<Integer>) object;

		System.out.println("number of sent packtes: " + sent.size());
		System.out.println("Number of Successful Packets: " + rec.size());
		//System.out.println("Successful sequence numbers: " + rec);
		for (int i : sent) {
			if (!rec.contains(i)) {
				notRec.add(i);
			}
		}
		if (notRec.size() == 0){drop = false;}
		System.out.println("Number of Unsuccesful Packets: " + notRec.size());
		//System.out.println("Unsuccessful sequence numbers: " +notRec);

	}
}