import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class TCPFileClient {

	public static void magicagain(int SOCKET_PORT,String SERVER,String FILE_TO_RECEIVED,int FILE_SIZE) throws UnknownHostException, IOException{
		int bytesRead;
		int current = 0;
	
		Socket sock = new Socket(SERVER, SOCKET_PORT);
		System.out.println("Connecting...");


		byte [] mybytearray  = new byte [FILE_SIZE];
		InputStream is = sock.getInputStream();
		FileOutputStream fos = new FileOutputStream(FILE_TO_RECEIVED);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		bytesRead = is.read(mybytearray,0,mybytearray.length);
		current = bytesRead;

		do {
			bytesRead =
					is.read(mybytearray, current, (mybytearray.length-current));
			if(bytesRead >= 0) current += bytesRead;
		} while(bytesRead > -1);

		bos.write(mybytearray, 0 , current);
		bos.flush();
		System.out.println("File " + FILE_TO_RECEIVED
				+ " downloaded (" + current + " bytes read)");

		fos.close();
		bos.close();
		sock.close();

	}

	public static void main (String [] args ) throws IOException {
		magicagain(8001,"localhost","srec.pdf",9171795);

	}







}
 
 
 